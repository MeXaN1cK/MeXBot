package mexan1ck.config

import java.io.FileInputStream
import java.util.*

object Config {
    private val prop = Properties()

    private const val IRC_NICK_KEY = "irc_nick"
    private const val IRC_NAME_KEY = "irc_name"
    private const val IRC_USER_KEY = "irc_user"
    private const val IRC_REALNAME_KEY = "irc_realname"
    private const val IRC_PASSWORD_KEY = "irc_password"
    private const val IRC_CHANNEL_KEY = "irc_channel"
    private const val IRC_HOST_KEY = "irc_host"
    private const val DISCORD_CHANNEL_KEY = "discord_channel"
    private const val DISCORD_TOKEN_KEY = "discord_token"
    private const val DISCORD_CHANNEL_WEBHOOK_URL = "discord_channelWebHookURL"
    private const val DISCORD_NAME = "discord_name"

    var irc_nick: String? = null
    var irc_channel: String? = null
    var irc_host: String? = null
    var irc_name: String? = null
    var irc_user: String? = null
    var irc_realname: String? = null
    var irc_password: String? = null
    var discord_channel: String? = null
    var discord_token: String? = null
    var discord_name: String? = null
    var discord_channelWebHookURL: String? = null

    fun load(filename: String) {
        FileInputStream(filename).use {
            prop.load(it)
            irc_nick = getString(IRC_NICK_KEY, "MeXDiscordBot")
            irc_name = getString(IRC_NAME_KEY, "MeXDiscordBot")
            irc_user = getString(IRC_USER_KEY, "MeXDiscordBot")
            irc_realname = getString(IRC_REALNAME_KEY, "MeX the Bot")
            irc_password = getString(IRC_PASSWORD_KEY, null)
            irc_channel = getString(IRC_CHANNEL_KEY, null)
            irc_host = getString(IRC_HOST_KEY, "irc.esper.net")
            discord_channel = getString(DISCORD_CHANNEL_KEY, null)
            discord_token = getString(DISCORD_TOKEN_KEY, null)
            discord_name = getString(DISCORD_NAME, "MeXBot")
            discord_channelWebHookURL = getString(DISCORD_CHANNEL_WEBHOOK_URL, null)
        }
    }

    private fun getString(key: String, default: String?): String? {
        return prop.getProperty(key) ?: default
    }
}
