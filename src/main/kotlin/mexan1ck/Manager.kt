package mexan1ck

import mexan1ck.message.Message

abstract class Manager {
    abstract fun send(message: Message)

    var listener: ((Message) -> Unit)? = null

    fun subscribe(listener: (Message) -> Unit) {
        this.listener = listener
    }
}
