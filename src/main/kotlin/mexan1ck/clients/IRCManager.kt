package mexan1ck.clients

import mexan1ck.Manager
import mexan1ck.log.Log
import mexan1ck.message.Message
import mexan1ck.message.MessageType
import net.engio.mbassy.listener.Handler
import org.kitteh.irc.client.library.Client
import org.kitteh.irc.client.library.event.channel.ChannelJoinEvent
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent
import org.kitteh.irc.client.library.event.channel.ChannelPartEvent
import org.kitteh.irc.client.library.event.connection.ClientConnectionClosedEvent
import org.kitteh.irc.client.library.event.connection.ClientConnectionEndedEvent
import org.kitteh.irc.client.library.event.connection.ClientConnectionFailedEvent
import org.kitteh.irc.client.library.event.user.UserQuitEvent
import org.kitteh.irc.client.library.feature.auth.NickServ
import org.kitteh.irc.client.library.feature.network.ClientConnection
import java.nio.ByteBuffer
import java.nio.CharBuffer
import java.nio.charset.Charset
import java.util.ArrayList
import kotlin.math.abs

private const val ZERO_WIDTH_SPACE = 0x200B.toChar()
private val CHARSET = Charsets.UTF_8
private const val MESSAGE_MAX_BYTES_LENGTH = 400

class IrcManager(host: String, private val channel: String,password: String, nick: String, name: String, user: String, realname: String): Manager()  {
    private val ircColors: Array<String> = arrayOf("\u0002","\u001D","\u001F","\u000399","\u000302","\u000303","\u000304","\u000305","\u000306","\u000307","\u000308","\u000309","\u000310","\u000311","\u000312","\u000313")
    private val client: Client = Client.builder()
            .nick(nick)
            .name(name)
            .user(user)
            .realName(realname)
            .server()
            .host(host)
            .then().buildAndConnect()
    init {
        client.eventManager.registerEventListener(this)
        client.authManager.addProtocol(NickServ.builder(client).account(nick).password(password).build()); // - auth on Nickserv
        client.addChannel(channel)
    }

    private fun hashOfNick(nickname: String): Int {
        val hash = nickname.hashCode()
        return (abs(hash) % ircColors.size) // Return index of color in ircColors from nickname
    }

    override fun send(message: Message) { // from Discord to IRC
        if (message.sender != "MeXBot") {
            val mesLenBytes = message.content.toByteArray(CHARSET).size
            when (message.type){
                MessageType.MESS_IMAGE -> {
                    message.image?.forEach {
                        client.sendMessage(channel, "${ircColors[hashOfNick(message.sender)]}${message.sender}\u000F: ${it.first}")
                    }
                }
                MessageType.MESS_IMAGE_TEXT -> {
                    message.image?.forEach {
                        client.sendMessage(channel, "${ircColors[hashOfNick(message.sender)]}${message.sender}\u000F: ${it.second} ${it.first}")
                    }
                }
                else -> {
                    if (mesLenBytes >= MESSAGE_MAX_BYTES_LENGTH){
                        val messArr = SplitStringByByteLength(message.content, CHARSET.toString(), MESSAGE_MAX_BYTES_LENGTH)
                        messArr.forEach {
                            client.sendMessage(channel, "${ircColors[hashOfNick(message.sender)]}${message.sender}\u000F: $it")
                        }
                    } else
                        client.sendMessage(channel, "${ircColors[hashOfNick(message.sender)]}${message.sender}\u000F: ${message.content}")
                }
            }
        }
    }

    private fun SplitStringByByteLength(src: String, encoding: String?, maxsize: Int): Array<String> {
        val cs = Charset.forName(encoding)
        val coder = cs.newEncoder()
        val out = ByteBuffer.allocate(maxsize) // output buffer of required size
        val `in` = CharBuffer.wrap(src)
        val ss: MutableList<String> = ArrayList() // a list to store the chunks
        var pos = 0
        while (true) {
            val cr = coder.encode(`in`, out, true) // try to encode as much as possible
            val newpos = src.length - `in`.length
            val s = src.substring(pos, newpos)
            ss.add(s) // add what has been encoded to the list
            pos = newpos // store new input position
            out.rewind() // and rewind output buffer
            if (!cr.isOverflow) {
                break // everything has been encoded
            }
        }
        return ss.toTypedArray()
    } // cut message by bytes

    // Messages from IRC to Discord \/

    @Handler
    fun onClientConnectionClose(event: ClientConnectionClosedEvent){
        Log.error("Connection close event! : ${event.cause}")
        event.client.reconnect()
    }

    @Handler
    fun onClientConnectionEnded(event: ClientConnectionEndedEvent){
        Log.error("Connection ended event! : ${event.cause}")
        event.client.reconnect()
    }

    @Handler
    fun onClientConnectionFailed(event: ClientConnectionFailedEvent){
        Log.error("Connection failed event! : ${event.cause}")
        event.client.reconnect()
    }

    @Handler
    fun onUserJoinChannel(event: ChannelJoinEvent) {
        if (listener != null){
            listener?.invoke(Message(event.actor.nick, " has joined to ${event.channel.name}.", MessageType.MESS_JOIN,null))
        }
    }

    @Handler
    fun onUserPartChannel(event: UserQuitEvent) {
        if (listener != null){
            listener?.invoke(Message(event.actor.nick, " has left $channel.", MessageType.MESS_QUIT,null))
        }
    }

    @Handler
    fun incoming(event: ChannelMessageEvent) {
        if (listener != null) {
            listener?.invoke(Message(event.actor.nick, event.message, MessageType.MESS_COMMON, null))
        }
    }

}
