package mexan1ck.clients

import mexan1ck.Manager
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import mexan1ck.message.Message
import mexan1ck.message.MessageType
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.entities.Member

class DiscordManager(token: String, private val channel: String) : Manager() {
    private val jda = JDABuilder(token)
        .addEventListeners(object: ListenerAdapter() {
            @Override
            override fun onMessageReceived(event: MessageReceivedEvent) {
                if (event.channel.name == channel) {
                    if (listener != null) {
                        var res = event.message.contentRaw
                        val mentedChannels = event.message.mentionedChannels
                        mentedChannels.forEach { it ->
                            res = res.replace(it.asMention, "#${it.name}")
                        }
                        val mentedMembers = event.message.mentionedMembers
                        mentedMembers.forEach { it ->
                            res = res.replace(it.asMention, "@${it.user.name}")
                        }
                        val mentedRoles = event.message.mentionedRoles
                        mentedRoles.forEach { it ->
                            res = res.replace(it.asMention, "@${it.name}")
                        }
                        val mentedUsers = event.message.mentionedUsers
                        mentedUsers.forEach { it ->
                            res = res.replace("<@!${it.id}>", "@${it.name}")
                        }
                        val imageAttach: List<Pair<String, String?>>
                        if (event.message.attachments.isNotEmpty()) {
                            val attach = event.message.attachments
                            imageAttach = attach.map { Pair(it.url, null) }
                            listener?.invoke(Message(event.author.name, res, MessageType.MESS_IMAGE, imageAttach))
                        } else if (event.message.attachments.isNotEmpty() && event.message.contentRaw.isNotEmpty()) {
                            val attach = event.message.attachments
                            imageAttach = attach.map { Pair(it.url, res) }
                            listener?.invoke(Message(event.author.name, res, MessageType.MESS_IMAGE_TEXT, imageAttach))
                        } else
                            listener?.invoke(Message(event.author.name, res, MessageType.MESS_COMMON, null))
                    }
                }
            }
        })
        .setActivity(Activity.watching("IRC"))
        .build()

    private fun MentionSomeone(message: String, users: List<Member>): String {
        val regex = "@([^\\s:,.;]+)".toRegex()
        var text = message.replace("@here","").replace("@everyone","")
        if(regex.containsMatchIn(text)) {
            text = regex.replace(text) { match ->
                val id = idByName(match.groupValues[1],users);
                if (id != null) "<@" + idByName(match.groupValues[1], users) + ">" else match.groupValues[0]
            }
        }
        return text
    }

    private fun idByName(name: String, users: List<Member>): String? = (users.find { it.user.name == name })?.id

    override fun send(message: Message) {
        jda.getTextChannelsByName(channel, false).map {
            val input = message.content
            val result = input.replace("@here","").replace("@everyone","")
            when (message.type){
                MessageType.MESS_JOIN, MessageType.MESS_QUIT -> it.sendMessage("``` " + message.sender + message.content + " ```").submit()
                else -> it.sendMessage("**${message.sender}**: ${MentionSomeone(result,it.members)}").submit()
            }
        }
    }
}