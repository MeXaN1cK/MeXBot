package mexan1ck

import mexan1ck.clients.DiscordManager
import mexan1ck.clients.IrcManager
import mexan1ck.config.Config

class MeXBot {
    init {
        Config.load("Config.properties")
        val ircManager = IrcManager(Config.irc_host!!, Config.irc_channel!!,Config.irc_password!!, Config.irc_nick!!, Config.irc_name!!, Config.irc_user!!, Config.irc_realname!!)
        val discordManager = DiscordManager(Config.discord_token!!, Config.discord_channel!!)
        ircManager.subscribe(discordManager::send)
        discordManager.subscribe(ircManager::send)
    }
}

fun main() {
    MeXBot()
}
