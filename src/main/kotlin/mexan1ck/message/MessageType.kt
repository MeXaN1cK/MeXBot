package mexan1ck.message

enum class MessageType {
    MESS_COMMON, MESS_QUIT, MESS_JOIN, MESS_IMAGE, MESS_IMAGE_TEXT
}