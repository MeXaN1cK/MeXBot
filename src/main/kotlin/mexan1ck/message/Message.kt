package mexan1ck.message

data class Message(val sender: String, val content: String, val type: MessageType? = MessageType.MESS_COMMON, val image: List<Pair<String, String?>>?)